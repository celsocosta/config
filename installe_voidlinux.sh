#!/usr/bin/bash

# REPOSITORIES
xbps-install -Svy void-repo-multilib void-repo-nonfree

# XORG PACKAGES
xbps-install -Svy xorg-minimal xorg-fonts xorg-input-drivers xorg-video-drivers xrandr setxkbmap xauth vdpauinfo libvdpau-va-gl font-misc-misc terminus-font dejavu-fonts-ttf dbus-elogind-x11 cups cups-devel cups-filters  font-awesome-4.7.0_3 font-awesome5-5.15.4_2 font-awesome6-6.1.1_1 
  
# KERNEL
xbps-install -Svy linux-firmware-network dkms linux-firmware

# SOUND
xbps-install -Svy alsa-plugins-pulseaudio paprefs pavucontrol pipewire alsa-utils gstreamer1-pipewire pipe-viewer mpv moc blueman mixxx youtube-dl

# NETWORK
xbps-install -Svy wget curl

# Libreoffice
xbps-install -Svy libreoffice-i18n-pt-BR-7.3.3.2_3 libreoffice-7.3.3.2_3 libreoffice-fonts-7.3.3.2_3 libreoffice-kit-7.3.3.2_3 libreoffice-xtensions-7.3.3.2_3 libreoffice-base-7.3.3.2_3

# DESKTOP ENVIRONMENT
xbps-install -Svy i3-gaps i3status dmenu urxvt-perls qt5ct qt5-styleplugins xdg-utils xdg-desktop-portal xdg-desktop-portal-gtk xdg-desktop-portal-kde xdg-user-dirs xdg-user-dirs-gtk libappindicator AppStream fluxbox feh xterm xcalc xlockmore numlockx simple-scan xcalc

# LOOK AND FEEL
xbps-install -Svy adwaita-icon-theme Adapta gnome-themes-standard numix-theme lxappearance

# FILE SYSTEM
xbps-install -Svy ntfs-3g exfat-utils fuse-exfat gvfs-afc gvfs-mtp udisks2 xfe ranger

# AUTH
xbps-install -Svy elogind dbus-elogind 

# DEVELOPMENT
xbps-install -Svy leafpad vscode nano vim cherrytree inkscape 

# ARCHIVE SOFTWARE
xbps-install -Svy dtrx p7zip

# POWER MANAGEMENT
xbps-install -Svy  upower

# UTILITIES
xbps-install -Svy htop bash-completion unzip btop

# INTERNET
xbps-install -Svy firefox irssi 

# SERVICES
ln -s /etc/sv/dbus /var/service
ln -s /etc/sv/elogind /var/service
ln -s /etc/sv/bluetoothd /var/service
ln -s /etc/sv/lightdm /var/service
ln -s /etc/sv/cupsd /var/service
ln -s /etc/sv/alsa /var/service

# GROUP SUDO
addgroup sudo

