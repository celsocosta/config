#!/bin/bash
# Criar links dos arquivos da home

ln -s ~/config/i3 ~/.config/
ln -s ~/config/bash ~/.bashrc
ln -s ~/config/nanorc ~/.nanorc
ln -s ~/config/Xresources ~/.Xdefaults
ln -s ~/config/fluxbox/ ~/.fluxbox
ln -s ~/config/vimrc ~/.vimrc
ln -s ~/config/muttrc ~/.muttrc
